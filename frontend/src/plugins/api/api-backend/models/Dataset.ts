/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DatasetDatabase } from './DatasetDatabase';
import type { DatasetGrouping } from './DatasetGrouping';
import type { DatasetInfo } from './DatasetInfo';

export type Dataset = {
  info: DatasetInfo;
  db: DatasetDatabase;
  grouping: DatasetGrouping;
  key: string;
  secret?: string;
  has_tiles: boolean;
};

