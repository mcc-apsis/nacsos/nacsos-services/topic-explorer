/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DatasetDatabase = {
  data: string;
  similarities: Array<string>;
  annotations: boolean;
};

