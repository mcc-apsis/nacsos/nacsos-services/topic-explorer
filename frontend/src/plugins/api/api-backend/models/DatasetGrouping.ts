/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DatasetGroupingBounds } from './DatasetGroupingBounds';

export type DatasetGrouping = {
  default_level?: DatasetGrouping.default_level;
  start?: DatasetGroupingBounds;
  end?: DatasetGroupingBounds;
  is_temporal?: boolean;
};

export namespace DatasetGrouping {

  export enum default_level {
    DAY = 'day',
    MONTH = 'month',
    YEAR = 'year',
  }


}

