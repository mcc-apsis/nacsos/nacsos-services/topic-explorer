/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DatasetGroupingBounds = {
  year: number;
  month: number;
  day: number;
};

