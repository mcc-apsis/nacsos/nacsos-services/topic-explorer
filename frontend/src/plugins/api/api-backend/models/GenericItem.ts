/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type GenericItem = {
  id: number;
  nacsos_id?: string;
  txt: string;
  topic: number;
  meta_topic: number;
  created: string;
  'x': number;
  'y': number;
  RowNum?: number;
  meta: any;
};

