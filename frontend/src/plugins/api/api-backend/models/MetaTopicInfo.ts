/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type MetaTopicInfo = {
  id: number;
  name: string;
  description?: string;
  cnt: number;
  share: number;
};

