/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TopicInfo = {
  id: number;
  name?: string;
  terms_tfidf?: string;
  terms_mmr?: string;
  cnt: number;
  share: number;
};

