/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Tweet = {
  id: number;
  nacsos_id?: string;
  txt: string;
  topic: number;
  meta_topic: number;
  created: string;
  'x': number;
  'y': number;
  RowNum?: number;
  twitter_id: string;
  retweets: number;
  likes: number;
  replies: number;
};

