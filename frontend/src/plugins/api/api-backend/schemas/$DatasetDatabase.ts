/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $DatasetDatabase = {
  properties: {
    data: {
      type: 'string',
      isRequired: true,
    },
    similarities: {
      type: 'array',
      contains: {
        type: 'string',
      },
      isRequired: true,
    },
    annotations: {
      type: 'boolean',
      isRequired: true,
    },
  },
} as const;
