/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $DatasetGrouping = {
  properties: {
    default_level: {
      type: 'Enum',
    },
    start: {
      type: 'DatasetGroupingBounds',
    },
    end: {
      type: 'DatasetGroupingBounds',
    },
    is_temporal: {
      type: 'boolean',
    },
  },
} as const;
