/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $DatasetGroupingBounds = {
  properties: {
    year: {
      type: 'number',
      isRequired: true,
    },
    month: {
      type: 'number',
      isRequired: true,
    },
    day: {
      type: 'number',
      isRequired: true,
    },
  },
} as const;
