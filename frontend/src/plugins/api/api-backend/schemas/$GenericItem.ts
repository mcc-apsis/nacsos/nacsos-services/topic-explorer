/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $GenericItem = {
  properties: {
    id: {
      type: 'number',
      isRequired: true,
    },
    nacsos_id: {
      type: 'string',
    },
    txt: {
      type: 'string',
      isRequired: true,
    },
    topic: {
      type: 'number',
      isRequired: true,
    },
    meta_topic: {
      type: 'number',
      isRequired: true,
    },
    created: {
      type: 'string',
      isRequired: true,
    },
    'x': {
      type: 'number',
      isRequired: true,
    },
    'y': {
      type: 'number',
      isRequired: true,
    },
    RowNum: {
      type: 'number',
    },
    meta: {
      properties: {
      },
      isRequired: true,
    },
  },
} as const;
