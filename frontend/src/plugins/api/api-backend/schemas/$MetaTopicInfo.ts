/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $MetaTopicInfo = {
  properties: {
    id: {
      type: 'number',
      isRequired: true,
    },
    name: {
      type: 'string',
      isRequired: true,
    },
    description: {
      type: 'string',
    },
    cnt: {
      type: 'number',
      isRequired: true,
    },
    share: {
      type: 'number',
      isRequired: true,
    },
  },
} as const;
