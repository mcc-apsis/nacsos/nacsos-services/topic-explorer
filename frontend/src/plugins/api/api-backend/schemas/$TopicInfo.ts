/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TopicInfo = {
  properties: {
    id: {
      type: 'number',
      isRequired: true,
    },
    name: {
      type: 'string',
    },
    terms_tfidf: {
      type: 'string',
    },
    terms_mmr: {
      type: 'string',
    },
    cnt: {
      type: 'number',
      isRequired: true,
    },
    share: {
      type: 'number',
      isRequired: true,
    },
  },
} as const;
