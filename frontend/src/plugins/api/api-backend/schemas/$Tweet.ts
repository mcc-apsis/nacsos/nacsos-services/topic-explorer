/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Tweet = {
  properties: {
    id: {
      type: 'number',
      isRequired: true,
    },
    nacsos_id: {
      type: 'string',
    },
    txt: {
      type: 'string',
      isRequired: true,
    },
    topic: {
      type: 'number',
      isRequired: true,
    },
    meta_topic: {
      type: 'number',
      isRequired: true,
    },
    created: {
      type: 'string',
      isRequired: true,
    },
    'x': {
      type: 'number',
      isRequired: true,
    },
    'y': {
      type: 'number',
      isRequired: true,
    },
    RowNum: {
      type: 'number',
    },
    twitter_id: {
      type: 'string',
      isRequired: true,
    },
    retweets: {
      type: 'number',
      isRequired: true,
    },
    likes: {
      type: 'number',
      isRequired: true,
    },
    replies: {
      type: 'number',
      isRequired: true,
    },
  },
} as const;
