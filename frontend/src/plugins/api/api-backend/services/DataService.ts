/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { GenericItem } from '../models/GenericItem';
import type { MetaTopicInfo } from '../models/MetaTopicInfo';
import type { TopicInfo } from '../models/TopicInfo';
import type { Tweet } from '../models/Tweet';

import type { CancelablePromise } from '@/plugins/api/core/CancelablePromise';
import type { BaseHttpRequest } from '@/plugins/api/core/BaseHttpRequest';

import type { ApiRequestOptions } from '@/plugins/api/core/ApiRequestOptions';

export class DataService {

  constructor(public readonly httpRequest: BaseHttpRequest) {}

  /**
   * Get Topics
   * @returns TopicInfo Successful Response
   * @throws ApiError
   */
  public getTopicsApiDataDatasetTopicsGet({
    dataset,
    head = false,
    secret,
  }: {
    dataset: string,
    head?: boolean,
    secret?: string,
  }, options?: Partial<ApiRequestOptions>): CancelablePromise<Array<TopicInfo>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/api/data/{dataset}/topics',
      path: {
        'dataset': dataset,
      },
      query: {
        'head': head,
        'secret': secret,
      },
      errors: {
        422: `Validation Error`,
      },
      ...options,
    });
  }

  /**
   * Get Meta Topics
   * @returns MetaTopicInfo Successful Response
   * @throws ApiError
   */
  public getMetaTopicsApiDataDatasetMetaTopicsGet({
    dataset,
    secret,
  }: {
    dataset: string,
    secret?: string,
  }, options?: Partial<ApiRequestOptions>): CancelablePromise<Array<MetaTopicInfo>> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/api/data/{dataset}/meta-topics',
      path: {
        'dataset': dataset,
      },
      query: {
        'secret': secret,
      },
      errors: {
        422: `Validation Error`,
      },
      ...options,
    });
  }

  /**
   * Get Topic Sample
   * @returns any Successful Response
   * @throws ApiError
   */
  public getTopicSampleApiDataDatasetSampleTopicGet({
    dataset,
    topic,
    limit = 50,
    secret,
  }: {
    dataset: string,
    topic: number,
    limit?: number,
    secret?: string,
  }, options?: Partial<ApiRequestOptions>): CancelablePromise<(Array<Tweet> | Array<GenericItem>)> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/api/data/{dataset}/sample/{topic}',
      path: {
        'dataset': dataset,
        'topic': topic,
      },
      query: {
        'limit': limit,
        'secret': secret,
      },
      errors: {
        422: `Validation Error`,
      },
      ...options,
    });
  }

  /**
   * Get Topic Paged
   * @returns any Successful Response
   * @throws ApiError
   */
  public getTopicPagedApiDataDatasetPagedTopicGet({
    dataset,
    topic,
    page = 1,
    limit = 50,
    secret,
  }: {
    dataset: string,
    topic: number,
    page?: number,
    limit?: number,
    secret?: string,
  }, options?: Partial<ApiRequestOptions>): CancelablePromise<(Array<Tweet> | Array<GenericItem>)> {
    return this.httpRequest.request({
      method: 'GET',
      url: '/api/data/{dataset}/paged/{topic}',
      path: {
        'dataset': dataset,
        'topic': topic,
      },
      query: {
        'page': page,
        'limit': limit,
        'secret': secret,
      },
      errors: {
        422: `Validation Error`,
      },
      ...options,
    });
  }

}
