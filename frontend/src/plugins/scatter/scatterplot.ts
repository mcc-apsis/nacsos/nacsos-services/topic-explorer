import * as THREE from 'three';
import * as d3 from 'd3';
import type {
  ColorMap,
  MouseEventCallback,
  PointReference,
  Points,
  PointsAttributes,
  FeatherSchema, Extent
} from '@/plugins/scatter/types';
import { TileManager } from '@/plugins/scatter/tiles';

export interface ScatterplotParameters {
  tileBaseUrl: string;
  canvas: HTMLCanvasElement;
  canvasBackground?: THREE.ColorRepresentation;
  // determines how the "thickness" of the cursor for mouse events
  raycasterThreshold?: number;

  // number of zoom levels (should be >= number of tile depth)
  numZoomLevels?: number;
  // zoom (k) marking the deepest zoom level
  maxZoomK?: number;

  // Column in the data to use as a label for colors
  colorField?: keyof FeatherSchema;
  // color scheme, has to provide the same labels as in `colorField`
  colorScheme?: ColorMap;

  // taking arbitrary x,y value space and scaling it to e.g. [1..100]
  pointRescale?: [number, number];
  // at what z-position to put the root layer
  basePlane?: number;
  // space between layers (tile depth) along z axis
  layerSpacing?: number;
  // frustum size of camera (i.e. left,right,top,bottom box boundary)
  frustumSize?: number;
  // depth (along z axis) the camera can see. Needs to be balanced with basePlane and layer spacing!
  cameraNear?: number;
  cameraFar?: number;

  // fallback color for points
  pointColor?: THREE.ColorRepresentation;
  // color for points when in focus
  pointColorHover?: THREE.ColorRepresentation;
  // size of points (in geometry)
  pointSize?: number;
  // size of points (in buffer)
  pointSizeBuffer?: number;
  // size of point in buffer when hovered
  pointSizeHover?: number;
  // balance between perspective and geometric zoom on point size on screen
  geomZoomShare?: number;
  // factor determining how fast point size changes in relation to zoom level
  pointSizeAdjuster?: number;
  // no clue, something with the perspective camera
  sizeAttenuation?: boolean;
  // called when cursor moves over a point (`d` is the index)
  mouseInCallback?: MouseEventCallback;
  // called when cursor moves away from a point (`d` is the index)
  mouseOutCallback?: MouseEventCallback;
  // called when clicked on a point (`d` is the index)
  mouseClickCallback?: MouseEventCallback;
}

type OptionalConfigFields =
  'colorField'
  | 'colorScheme'
  | 'mouseInCallback'
  | 'mouseOutCallback'
  | 'mouseClickCallback'

export class Scatterplot {
  public readonly canvas: HTMLCanvasElement;
  public readonly height: number;
  public readonly width: number;

  private readonly scene: THREE.Scene;
  private readonly camera: THREE.PerspectiveCamera | THREE.OrthographicCamera;
  private readonly renderer: THREE.WebGLRenderer;
  private readonly pointsMaterial: THREE.PointsMaterial;

  private tileManager: TileManager;

  private conf: Required<Omit<ScatterplotParameters, 'tileBaseUrl' | 'canvas' | OptionalConfigFields>> &
    Pick<ScatterplotParameters, OptionalConfigFields>;

  // id (offset in buffer) of point that is in focus
  private focusedPoint?: PointReference;
  // id (offset in buffer) of point that is selected
  private selectedPoint?: PointReference;

  public zoomHandler: d3.ZoomBehavior<HTMLCanvasElement, unknown>;

  constructor(params: ScatterplotParameters) {
    if (!params.canvas) throw new Error('You have to provide a canvas!');
    if (!params.tileBaseUrl) throw new Error('You have to provide a base URL for tiles!');
    this.conf = {
      canvasBackground: params.canvasBackground ?? 0x2f2f2f,
      raycasterThreshold: params.raycasterThreshold ?? 0.05,
      pointColor: params.pointColor ?? 0x00EE00,
      pointColorHover: params.pointColorHover ?? 0xFF0000,
      pointSize: params.pointSize ?? 1.3,
      pointSizeBuffer: params.pointSizeBuffer ?? 1,
      pointSizeHover: params.pointSizeHover ?? 2,
      geomZoomShare: params.geomZoomShare ?? 0.4,
      pointSizeAdjuster: params.pointSizeAdjuster ?? .85,
      sizeAttenuation: params.sizeAttenuation ?? true,
      numZoomLevels: params.numZoomLevels ?? 8,
      maxZoomK: params.maxZoomK ?? 800,
      colorField: params.colorField,
      colorScheme: params.colorScheme,
      mouseInCallback: params.mouseInCallback,
      mouseOutCallback: params.mouseOutCallback,
      mouseClickCallback: params.mouseClickCallback,
      layerSpacing: params.layerSpacing ?? 25,
      basePlane: params.basePlane ?? 1000,
      frustumSize: params.frustumSize ?? 400,
      pointRescale: params.pointRescale ?? [0, 100],
      cameraNear: params.cameraNear ?? 0.1,
      cameraFar: params.cameraFar ?? 1000,
    };

    this.canvas = params.canvas;

    const { height, width } = this.canvas.getBoundingClientRect();
    this.height = height;
    this.width = width;

    this.pointsMaterial = this.initPointsMaterial();
    this.scene = this.initScene();
    this.camera = this.initCamera();
    this.renderer = this.initRenderer();

    this.zoomHandler = this.initZoomHandler();
    this.initMouseMoveHandler();

    this.tileManager = new TileManager({
      baseUrl: params.tileBaseUrl,
      onUpdate: this.handleTileUpdate.bind(this),
      onNew: this.handleNewTiles.bind(this),
      conf: {
        pointColor: this.conf.pointColor,
        pointSize: this.conf.pointSize,
        pointSizeBuffer: this.conf.pointSizeBuffer,
        material: this.pointsMaterial,
        colorField: this.conf.colorField,
        colorScheme: this.conf.colorScheme,
        basePlane:this.conf.basePlane,
        pointRescale:this.conf.pointRescale,
        layerSpacing:this.conf.layerSpacing
      }
    });
    setTimeout(() => this.tileManager.addTile({ depth: 1, coord0: 0, coord1: 0 }), 1000);
    setTimeout(() => this.tileManager.addTile({ depth: 1, coord0: 0, coord1: 1 }), 2000);
    setTimeout(() => this.tileManager.addTile({ depth: 1, coord0: 0, coord1: 2 }), 3000);
    setTimeout(() => this.tileManager.addTile({ depth: 1, coord0: 0, coord1: 3 }), 4000);
  }

  initScene() {
    const scene = new THREE.Scene();
    scene.background = new THREE.Color(this.conf.canvasBackground);
    // scene.fog = new THREE.FogExp2(0x000000, .014);
    return scene;
  }

  initRenderer() {
    const renderer = new THREE.WebGLRenderer({ antialias: true, canvas: this.canvas });
    renderer.setSize(this.width, this.height);
    renderer.setPixelRatio(devicePixelRatio);
    return renderer;
  }

  initCamera() {
    const aspect = this.width / this.height;
    // return new THREE.PerspectiveCamera(90, this.width / this.height, 1, 1000);
    // const camera = new THREE.OrthographicCamera(this.width / -2, this.width / 2, this.height / 2, this.height / -2, 1, 1000);
    // const camera = new THREE.OrthographicCamera(0, this.width , 0, this.height, .1, 1000);
    const camera = new THREE.OrthographicCamera(
      this.conf.frustumSize * aspect / -2,
      this.conf.frustumSize * aspect / 2,
      this.conf.frustumSize / 2,
      this.conf.frustumSize / -2,
      this.conf.cameraNear, this.conf.cameraFar);

    camera.position.set(
      (this.conf.pointRescale[1] - this.conf.pointRescale[0]) / 2,
      (this.conf.pointRescale[1] - this.conf.pointRescale[0]) / 2,
      this.conf.basePlane + this.conf.cameraFar - this.conf.layerSpacing / 2);
    return camera;

  }

  initPointsMaterial() {
    const size = 32; // should always be an odd number (so div 2 gives int centre)
    const data = new Uint8Array(4 * size * size);
    let distance: number;
    for (let run = 0; run < (size * size); run++) {
      // draw a filled circle
      // xy centre: ((size - 1) / 2)
      // current col (x): run % size
      // current row (y): Math.floor(run / size)
      // a² + b² = c²
      distance = Math.sqrt(((((size - 1) / 2) - Math.floor(run / size)) ** 2) + ((((size - 1) / 2) - (run % size)) ** 2));
      if (distance < (((size - 1) / 2) - 1)) {
        data.set([255, 255, 255, 255], run * 4);
      } else {
        data.set([0, 0, 0, 0], run * 4);
      }
    }
    const texture = new THREE.DataTexture(data, size, size);
    texture.needsUpdate = true;
    const material = new THREE.PointsMaterial({
      size: this.conf.pointSize * this.conf.pointSizeAdjuster,
      sizeAttenuation: this.conf.sizeAttenuation,
      vertexColors: true, // enables setting color via geometry attributes (ArrayBuffer)
      alphaMap: texture,
      blending: THREE.AdditiveBlending,
      depthTest: false,
      transparent: true
    });

    // customise the shader function to accommodate setting the size for each point individually
    material.onBeforeCompile = shader => {
      shader.vertexShader = shader.vertexShader
        .replace('uniform float size;', 'attribute float size;');
    };
    return material;
  }

  initZoomHandler() {
    const zoom2depth = d3.scaleQuantize()
      .domain([1, this.conf.maxZoomK])
      .range([...Array(this.conf.numZoomLevels).keys()]);

    const handleZoomEvent = (transform: d3.ZoomTransform) => {
      const { x: transX, y: transY, k: scale } = transform;
      // this.scene.children = Object.entries(this.tileManager.tiles).forEach((value)=>{
      //   const {key, tile}=value;
      //
      // })

      // TODO filter visible point clouds in scene by zoom level
      // TODO determine viewport extent on z=0 plane
      // TODO tell tile manager about the updated depth and viewport
      //      (TM should figure out itself which tile if any is needed)

      // console.log(transX, transY, scale, this.camera.position);

      // this enables scrolling into a certain region and moving the focus centre
      // move camera towards/away from the scene and change field of view
      // see why this improves the zooming experience here
      // https://observablehq.com/@bmschmidt/zoom-strategies-for-huge-scatterplots-with-three-js#cell-1821
      // this.camera.fov = this.getCameraFov(scale);
      // this.camera.position.z = this.getCameraZ(scale);
      this.camera.zoom = scale / 2;
      this.camera.position.setX(-(transX - (this.width / 2)) / scale);
      this.camera.position.setY((transY - (this.height / 2)) / scale);
      this.camera.position.setZ(this.conf.basePlane + this.conf.cameraFar - this.conf.layerSpacing / 2 - scale);

      // update the camera matrices
      this.camera.updateProjectionMatrix();

      // tell renderer to update
      this.redraw();
    };

    const zoom = d3.zoom<HTMLCanvasElement, unknown>()
      .scaleExtent([1, 40000])
      .extent([[0, 0], [this.width, this.height]])
      .on('zoom', ({ transform }) => handleZoomEvent(transform));

    d3.select(this.canvas).call(zoom);

    return zoom;
  }

  initMouseMoveHandler() {
    const raycaster = new THREE.Raycaster();
    raycaster.params.Points = { threshold: this.conf.raycasterThreshold };

    const resetAttributes = (points: Points, ix: number) => {
      const attributes = points.geometry.attributes as unknown as PointsAttributes;

      // reset color to default
      const c = new THREE.Color(this.conf.pointColor);
      attributes.color.setXYZ(ix, c.r, c.g, c.b);
      attributes.color.needsUpdate = true;
      // reset size to default
      attributes.size.setX(ix, this.conf.pointSize);
      attributes.size.needsUpdate = true;

      // reset Z offset to 0
      attributes.position.setZ(ix, 0);
      attributes.position.needsUpdate = true;
    };

    const setAttributes = (points: Points, ix: number,) => {
      const attributes = points.geometry.attributes as unknown as PointsAttributes;

      // change color
      const c = new THREE.Color(this.conf.pointColorHover);
      attributes.color.setXYZ(ix, c.r, c.g, c.b);
      attributes.color.needsUpdate = true;

      // increase size
      attributes.size.setX(ix, this.conf.pointSizeHover);
      attributes.size.needsUpdate = true;

      // move closer to camera (so it's definitely in front)
      attributes.position.setZ(ix, 0.001);
      attributes.position.needsUpdate = true;
    };

    const mouseMoveHandler = (event: MouseEvent) => {
      event.preventDefault();

      const mousePosition = new THREE.Vector2(
        (event.offsetX / this.canvas.width) * 2 - 1,
        -(event.offsetY / this.canvas.height) * 2 + 1);

      raycaster.setFromCamera(mousePosition, this.camera);
      const intersects = raycaster.intersectObjects<THREE.Points<THREE.BufferGeometry, THREE.PointsMaterial>>(this.scene.children);

      const { index: ix, object: points } = intersects[0] || {};
      const { name: key } = points || {};

      if (this.focusedPoint && (this.focusedPoint.ix !== ix || this.focusedPoint.points.name !== key)) {
        // something was in focus, but now it should be something else
        // revert to default attributes
        resetAttributes(this.focusedPoint.points, this.focusedPoint.ix);
        // tell whoever cares that focus will be lost
        if (this.conf.mouseOutCallback) this.conf.mouseOutCallback(this.focusedPoint.points.name, this.focusedPoint.ix);
        // unmark intersection
        this.focusedPoint = undefined;
      }
      if (ix && points && key) {
        // set attributes to the "focus" state
        setAttributes(points, ix);
        // remember which element this was, so we can reset the attributes later on
        this.focusedPoint = { points, ix };
        // tell whoever cares that a new point is in focus
        if (this.conf.mouseInCallback) this.conf.mouseInCallback(key, ix);
      }

      this.redraw();
    };

    this.canvas.addEventListener('mousemove', mouseMoveHandler);
  }

  handleTileUpdate() {
    // TODO: remove tiles from scene
  }

  handleNewTiles(points: Points) {
    this.scene.add(points);
    this.redraw();
  }

  redraw() {
    this.renderer.render(this.scene, this.camera);
  }

  getCameraFov(scale: number) {
    const fovHeight = this.height / scale;
    const halfFOVradians = Math.atan(fovHeight / (2 * this.camera.position.z));
    const halfFOV = toDegrees(halfFOVradians);
    return halfFOV * 2;
  }

  getCameraZ(zoomLevel: number) {
    return this.height / Math.exp(Math.log(zoomLevel) * this.conf.geomZoomShare);
  }

}

function toDegrees(angle: number): number {
  return angle * (180 / Math.PI);
}

function toRadians(angle: number): number {
  return angle * (Math.PI / 180);
}