import type { Float32, Uint16, Uint32, Uint64 } from 'apache-arrow';
import type { BufferAttribute } from 'three/src/core/BufferAttribute';
import type { InterleavedBufferAttribute } from 'three/src/core/InterleavedBufferAttribute';
import type * as THREE from 'three';

export interface TileReference {
  depth: number;
  coord0: number;
  coord1: number;
}

export interface Extent {
  x: [number, number];
  y: [number, number];
}

export type Color = [number, number, number];
export type ColorMap = Record<number, THREE.Color>;

export interface TileMetadata {
  extent: Extent;
  children: string[];
  total_points: number;
  tile_size: number;
}

export type FeatherSchema = {
  ix: Uint64,
  twitter_id: Uint64,
  author_id: Uint64,
  topic: Uint16,
  meta_topic: Uint16,
  retweets: Uint32,
  replies: Uint32,
  likes: Uint32,
  x: Float32,
  y: Float32,
}

export interface PointsAttributes {
  size: BufferAttribute | InterleavedBufferAttribute;
  color: BufferAttribute | InterleavedBufferAttribute;
  position: BufferAttribute | InterleavedBufferAttribute;
}

export type MouseEventCallback = (key: string, ix: number) => void;
export type Points = THREE.Points<THREE.BufferGeometry, THREE.PointsMaterial>;
export type PointReference = { points: Points, ix: number };