import { ref } from 'vue';
import { defineStore } from 'pinia';
import type { Dataset } from '@/plugins/api/api-backend';
import { API } from '@/plugins/api';

export const useDatasetStore = defineStore('dataset', () => {
  const datasets = ref<Record<string, Dataset>>({});
  const dataset = ref<Dataset | null>(null);

  function loadDatasets() {
    return new Promise((res, rej) => {
      API.datasets.getDatasetsApiDatasetsGet()
        .then((response) => {
          datasets.value = {};
          response.data.forEach((dataset_) => {
            datasets.value[dataset_.key] = dataset_;
          });
          res(null);
        })
        .catch(() => {
          rej();
        });
    });
  }

  function select(selectedDataset: string) {
    return new Promise((res, rej) => {
      if (Object.keys(datasets.value).length === 0) {
        loadDatasets().then(() => {
          dataset.value = datasets.value[selectedDataset];
          res(dataset.value);
        }).catch(() => {
          rej();
        });
      } else {
        dataset.value = datasets.value[selectedDataset];
        res(dataset.value);
      }
    });
  }

  return { select, loadDatasets, dataset, datasets };
});
