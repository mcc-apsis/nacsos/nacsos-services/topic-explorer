import { useDatasetStore } from '@/stores/dataset';
import { createPinia } from 'pinia';
import router from '@/router';
import { markRaw } from 'vue';

const pinia = createPinia();
pinia.use(({ store }) => {
  store.router = markRaw(router);
});
export const datasetStore = useDatasetStore(pinia);

export default pinia;
