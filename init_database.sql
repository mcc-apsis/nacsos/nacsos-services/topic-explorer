CREATE TABLE topics (
    id          INTEGER PRIMARY KEY NOT NULL,
    name        TEXT,
    terms_tfidf TEXT,
    terms_mmr   TEXT
);

CREATE TABLE meta_topics (
    id          INTEGER PRIMARY KEY NOT NULL,
    name        TEXT            NOT NULL,
    description TEXT
);

CREATE TABLE similarities (
    topic_a    INTEGER  NOT NULL,
    topic_b    INTEGER  NOT NULL,
    metric     TEXT NOT NULL,
    similarity REAL NOT NULL,
    UNIQUE (topic_a, topic_b, metric) ON CONFLICT REPLACE,
    FOREIGN KEY (topic_a) REFERENCES topics (id),
    FOREIGN KEY (topic_b) REFERENCES topics (id)
);
CREATE INDEX ix_similarities_topic_a ON similarities (topic_a);

CREATE TABLE tweets (
    id         INTEGER PRIMARY KEY AUTOINCREMENT,
    nacsos_id  TEXT NOT NULL,
    twitter_id TEXT NOT NULL,
    author_id  TEXT NOT NULL,
    txt        TEXT NOT NULL,
    created    TEXT NOT NULL,
    topic      INTEGER,
    meta_topic INTEGER,
    retweets   INTEGER  NOT NULL,
    likes      INTEGER  NOT NULL,
    replies    INTEGER  NOT NULL,
    group_year CHAR(4),
    group_month CHAR(7),
    group_day  CHAR(10),
    x          FLOAT NOT NULL,
    y          FLOAT NOT NULL,
    FOREIGN KEY (topic) REFERENCES topics (id),
    FOREIGN KEY (meta_topic) REFERENCES meta_topics (id)
);
CREATE INDEX ix_tweets_nacsos_id ON tweets (twitter_id);
CREATE INDEX ix_tweets_twitter_id ON tweets (twitter_id);
CREATE INDEX ix_tweets_topic ON tweets (topic);
CREATE INDEX ix_tweets_meta_topic ON tweets (meta_topic);
CREATE INDEX ix_tweets_group_year ON tweets (group_year);
CREATE INDEX ix_tweets_group_month ON tweets (group_month);
CREATE INDEX ix_tweets_group_day ON tweets (group_day);

CREATE VIRTUAL TABLE tweets_fts USING fts5(txt, content=tweets, content_rowid=id);

-- CREATE TRIGGER trg_tweets_insert
--     INSTEAD OF INSERT ON tweets
--     BEGIN
--         INSERT INTO tweets
--             (nacsos_id, twitter_id, author_id, txt, created, topic, meta_topic, retweets, likes, replies, group_year, group_month, group_day, x, y)
--         VALUES
--             (NEW.nacsos_id, NEW.twitter_id, NEW.author_id, NEW.txt, NEW.created, NEW.topic, NEW.meta_topic, NEW.retweets, NEW.likes, NEW.replies, substring(NEW.created, 0, 5), substring(NEW.created, 0, 8), substring(NEW.created, 0, 11), NEW.x, NEW.y);
--     END;



-- CREATE TABLE generic (
--     id         INT PRIMARY KEY AUTOINCREMENT,
--     nacsos_id  TEXT NOT NULL,
--     txt        TEXT NOT NULL,
--     created    TEXT NOT NULL,
--     meta       TEXT,
--     topic      INT,
--     meta_topic INT,
--     group_year CHAR(4),
--     group_month CHAR(7),
--     group_day  CHAR(10),
--     x          FLOAT NOT NULL,
--     y          FLOAT NOT NULL,
--     FOREIGN KEY (topic) REFERENCES topics (id),
--     FOREIGN KEY (meta_topic) REFERENCES meta_topics (id)
-- );



-- CREATE VIEW grp_year AS
--     SELECT substring(created, 0, 5) as grp, count(1) as cnt
--     FROM tweets
--     GROUP BY substring(created, 0, 5);
--
-- CREATE VIEW grp_month AS
--     SELECT substring(created, 0, 8) as grp, count(1) as cnt
--     FROM tweets
--     GROUP BY substring(created, 0, 8);
--
-- CREATE VIEW grp_day AS
--     SELECT substring(created, 0, 11) as grp, count(1) as cnt
--     FROM tweets
--     GROUP BY substring(created, 0, 11);