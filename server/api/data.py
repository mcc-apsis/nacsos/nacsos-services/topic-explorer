from fastapi import APIRouter
import logging

from ..utils.sqlite import Database
from ..utils.types import Tweet, GenericItem, TopicInfo, MetaTopicInfo

logger = logging.getLogger('api.data')
router = APIRouter()


@router.get('/topics', response_model=list[TopicInfo])
async def get_topics(dataset: str, head: bool | None = False, secret: str | None = None):
    with Database(dataset, secret) as db:
        q = db.resolve('''
        WITH topic_counts AS (SELECT topic, count(1) as cnt
                      FROM $data_tab
                      GROUP BY topic)
        SELECT topics.id, topics.name, topics.terms_tfidf, topics.terms_mmr,
               topic_counts.cnt as cnt,
               CAST(topic_counts.cnt as REAL) / (SELECT count(1) FROM $data_tab) as share
        FROM topics,
             topic_counts
        WHERE topic_counts.topic = topics.id
        ''')
        if head:
            q += ' LIMIT 15'
        res = db.cur.execute(q)
        return [TopicInfo.parse_obj(r) for r in res]


@router.get('/meta-topics', response_model=list[MetaTopicInfo])
async def get_meta_topics(dataset: str, secret: str | None = None):
    with Database(dataset, secret) as db:
        q = db.resolve('''
        WITH meta_topic_counts AS (SELECT meta_topic, count(1) as cnt
                      FROM $data_tab
                      GROUP BY meta_topic)
        SELECT meta_topics.id, meta_topics.name, meta_topics.description,
               meta_topic_counts.cnt as cnt,
               CAST(meta_topic_counts.cnt as REAL) / (SELECT count(1) FROM $data_tab) as share
        FROM meta_topics,
             meta_topic_counts
        WHERE meta_topic_counts.meta_topic = meta_topics.id
        ''')
        res = db.cur.execute(q)
        return [MetaTopicInfo.parse_obj(r) for r in res]


@router.get('/sample/{topic}',
            response_model=list[Tweet] | list[GenericItem],
            response_model_exclude={'x', 'y', 'RowNumber'})
async def get_topic_sample(dataset: str, topic: int, limit: int = 50, secret: str | None = None):
    with Database(dataset, secret) as db:
        q = db.resolve('SELECT * '
                       'FROM (SELECT ROW_NUMBER() OVER (ORDER BY created ) RowNum, * '
                       '      FROM $data_tab '
                       '      WHERE topic = :topic) '
                       'WHERE RowNum % floor((SELECT count(1) FROM $data_tab WHERE topic = :topic) / :limit) = 0;')
        res = db.cur.execute(q, {'topic': topic, 'limit': limit})

        if db.dataset.info.type == 'twitter':
            return [Tweet(**r) for r in res]
        else:
            return [GenericItem.parse_obj(r) for r in res]


@router.get('/paged/{topic}',
            response_model=list[Tweet] | list[GenericItem],
            response_model_exclude={'x', 'y', 'RowNumber'})
async def get_topic_paged(dataset: str, topic: int, page: int = 1, limit: int = 50, secret: str | None = None):
    with Database(dataset, secret) as db:
        q = db.resolve('SELECT * '
                       'FROM $data_tab '
                       'WHERE topic = :topic '
                       'LIMIT :limit OFFSET :offset;')
        res = db.cur.execute(q, {'topic': topic, 'limit': limit, 'offset': (page - 1) * limit})

        if db.dataset.info.type == 'twitter':
            return [Tweet(**r) for r in res]
        else:
            return [GenericItem.parse_obj(r) for r in res]


def get_histogram():
    q = '''
    WITH t_all AS (SELECT group_month, count(1) as cnt
               FROM tweets
               GROUP BY group_month),
         t_sel AS (SELECT group_month, count(1) as cnt
                   FROM tweets
                   WHERE topic = 342
                   GROUP BY group_month)
    SELECT t_all.group_month, coalesce(CAST(t_sel.cnt as FLOAT), 0.0) / t_all.cnt as share
    FROM t_all
             LEFT OUTER JOIN t_sel ON t_sel.group_month = t_all.group_month;
    '''
