from pydantic import BaseModel


class BaseItem(BaseModel):
    id: int
    nacsos_id: str | None
    txt: str
    topic: int
    meta_topic: int
    created: str
    x: float
    y: float

    # for internal use
    RowNum: int | None = None


class Tweet(BaseItem):
    twitter_id: str
    retweets: int
    likes: int
    replies: int


class GenericItem(BaseItem):
    meta: dict


class Topic(BaseModel):
    id: int
    name: str | None
    terms_tfidf: str | None
    terms_mmr: str | None


class TopicInfo(Topic):
    cnt: int
    share: float


class MetaTopic(BaseModel):
    id: int
    name: str
    description: str | None


class MetaTopicInfo(MetaTopic):
    cnt: int
    share: float


class Similarity(BaseModel):
    topic_a: int
    topic_b: int
    metric: str
    similarity: float
